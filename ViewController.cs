﻿using Foundation;
using System;
using System.Linq;
using ARKit;
using SceneKit;
using UIKit;
using System.Threading.Tasks;

namespace ScenekitModel
{
    public partial class ViewController : UIViewController
    {
        private readonly ARSCNView sceneView;

        public ViewController(IntPtr handle) : base(handle)
        {
            this.sceneView = new ARSCNView
            {
                AutoenablesDefaultLighting = true
            };

            this.View.AddSubview(this.sceneView);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            this.sceneView.Frame = this.View.Frame;
        }

        public override async void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            this.sceneView.Session.Run(new ARWorldTrackingConfiguration
            {
                AutoFocusEnabled = true,
                PlaneDetection = ARPlaneDetection.Horizontal,
                LightEstimationEnabled = true,
                WorldAlignment = ARWorldAlignment.Gravity

            }, ARSessionRunOptions.ResetTracking | ARSessionRunOptions.RemoveExistingAnchors);

            await Task.Delay(3000);

            this.sceneView.Scene.RootNode.AddChildNode(CreateModelNodeFromFile("art.scnassets/tertis/blender.obj"));
        }

        public static SCNNode CreateModelNodeFromFile(string filePath)
        {
            var sceneFromFile = SCNScene.FromFile(filePath);

            

            var model = sceneFromFile.RootNode.ChildNodes.First(x=>x.Geometry != null);//.FindChildNode("andy", true);
            model.Geometry.FirstMaterial.Emission.Contents = UIColor.White;
            //model.Scale = new SCNVector3(0.02f, 0.4f, 0.02f);
            model.Position = new SCNVector3(0, -0.2f, 0);

            return model;
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);

            this.sceneView.Session.Pause();
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
        }
    }
}